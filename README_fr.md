Abelujo - logiciel libre de gestion de petite librairie indépendante (et disquaires)
====================================================================

Projet actuellement en chantier. Il est néanmoins possible de:

-   rechercher des **livres** par mots-clefs ou par n°ISBN/EAN. La recherche
    utilise les résultats de [la librairie Decitre](https://www.decitre.fr), ou bien Chapitre.com,
-   rechercher des **CDs** (via [discogs.com](http://www.discogs.com)),
-   chercher des notices espagnoles ou allemandes,
- ajouter des exemplaires au stock, par lieu, utiliser des paniers,
-   chercher dans son stock,
-   effectuer des ventes, voir les conflits à la vente, voir l'historique (global, par notice),
-   créer des dépôts, **faire un état de dépôt**,
- lancer un **inventaire** par lieu,
-   importer des données depuis un fichier **LibreOffice Calc** (.ods). (expérimental. Voir la [documentation utilisateur](doc/french/index.rst "doc utilisateur")).

L'application est traduite en français, anglais et espagnol (castillan).

Nous nous basons sur les spécifications fonctionnelles du projet Ruche,
auxquelles nous avons participé:
<http://ruche.eu.org/wiki/Specifications_fonctionnelles>. Nous avons
écrit ce que nous avons compris de la gestion des dépôts, des commandes,
etc. N'hésitez donc pas à les lire et à nous dire si ce que nous
commençons correspondra à vos besoins.

**Abelujo** signifie Ruche en Espéranto.

Installation et développement
=============================

Nous vous laissons le soin de [lire les instructions en anglais](README.md "").

Ci-dessous une capture d'écran de la recherche dans la base de données:

![chercher une notice](doc/abelujo-collection.png)
