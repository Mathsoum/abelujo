��    V      �     |      x     y     �     �     �     �  	   �     �     �     �       
             %     -     :     C     O     \     n     s     �     �     �     �     �     �     �     �     �     	  
   	     )	     8	     P	     Y	     m	  	   y	     �	  
   �	     �	     �	  
   �	     �	     �	     �	     �	     �	     
     
     
     '
     :
  0   R
  %   �
  C   �
  1   �
  (     "   H     k  #   �     �  
   �     �     �  2   �            @   )     j     y     �     �     �     �     �     �     �     �     �     �  	   �     �     �            @  4     u     �     �  	   �     �     �     �     �  $         %  	   -  	   7     A     I     Y  	   b  
   l     w     �     �     �     �     �     �  
   �     �               #     7     P     \     q  	   �     �     �     �     �     �     �     �  	     	          	   2     <     S     \  )   c     �     �     �  &   �  '   �  L     ?   _  5   �     �  "   �  %        =     C     O     T  D   c     �  %   �  =   �          +     >     F     N  	   _     i     r     {     �     �     �     �     �  
   �     �     �     :       I   #       4      H             &      !   $          =          L       ,                      D   )   <             /   ?   
       U   J   6   5                     E   T   N      >   S                      -   P          A   C          V              ;          %   R   .   K                  Q   "                 F      '              3       B       G       9   7      0                 	   2   M   +   O                                    1   *   @   (   8    Add a card manually Add this card to the basket Add to the deposit: Alert of the Alerts All cards Authors Auto command Auto-command the cards Cancel Castellano Created the Deposit Deposit type Deposits Distributor Distributors Django site admin Edit Edit the card English Error when adding the deposit Exemplaries in stock German History Inital quantity Inventories Inventory ongoing Inventory started Last deposit state Look for a Look for cards Minimum wanted quantity My stock Name of the deposit New deposit New entry No card is this basket No results On the place Presente in Publishers Quantity Quantity left Search Search in the stock See on Sell Sell successfull. Selling cards Start an inventory The card doesn't exist. The card were successfully added to the deposit. The deposit was successfully created. The deposit wasn't created. It must contain at least one valid card The isbn will be downloaded when you press enter. The search by ean isn't implemented yet. There is a conflict with deposits. This deposit doesn't exist ! This deposit doesn't have any card. Title To command Type Type of card Use TAB to validate your choice, Enter to proceed. Validate Warning: we couldn't sell {}. We could not add the card to the deposit: the given ean is null. about the card add to deposit cards conflict deposit state distributor edit in stock never or places produit indisponible publisher related deposits results titles selected. Total: whose distributor is Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-30 10:11-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Ajouter une notice manuellement Ajouter cette notice au panier Ajouter au dépôt: Alerte du Alertes Toutes notices Auteurs Auto-commander Marquer à commander automatiquement Annuler Castillan Créé le Dépôt Tipe de dépôt Dépôts Diffuseur Diffuseurs Admin de Django Édition Modifier la notice Anglas Erreur en ajoutant le dépôt Exemplaires en stock Allemand Historique Quantity initiale Inventaires Inventaire en cours Inventaire en cours Dernier état de dépôt Chercher un Chercher des notices Quantité minimum demandée Mon stock Nom du dépôt Nouveau dépôt Nouvelle entrée Aucun exemplaire dans ce panier Aucun résultat Lieu Présente dans Éditeurs Quantité Quantité restante Recherche Chercher dans le stock Voir sur Vendre La vente a été réalisée avec succès. Vente Commencer un inventaire La notice n'existe pas. La notice a été ajoutée au dépôt. Le dépôt a été créé avec succès. Le dépôt n'a pas été créé. Il doit contenir au moins une notice valide Le n°isbn sera téléchargé à l'enregistrement de la notice. La recherche par isbn n'est pas encore implémentée. Il y a un conflit de dépôts. Le dépôt demandé n'existe pas ! Ce dépôt ne contient aucune notice. Titre A commander Tipe Tipe de notice Utilisez TAB pour valider la selection, Entrée pour finir la vente. Valider Attention: la vente de {} a échoué. Impossible d'ajouter cette notice au dépôt: l'isbn est nul. à propos de la notice ajouter au dépôt notices conflit état de dépôt diffuseur modifier en stock jamais ou lieus de stockage produit indisponible éditeur dépôts concernés résultats titres sélectionnés. Total: dont le distributeur est 