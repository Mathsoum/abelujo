��    f      L  �   |      �     �     �     �     �  	   	     	     	      	  	   '	     1	     H	     P	     ]	     t	     |	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     
     
     "
  	   7
     A
     H
     P
     `
  	   l
  	   v
     �
  
   �
     �
     �
     �
     �
     �
  	   �
     �
  
                  -  %   :     `     g     s  
   �     �     �     �     �     �     �     �     �     �  %   �     "  "   :  %   ]  C   �  (   �     �     
  
                2   -     `     i  3   �     �     �     �     �     �     �                          &  #   /     S     V     ]  	   r     |     �     �     �     �     �     �     �  @  �     9     W     v     �     �  
   �     �     �     �     �     �               #     *     3  	   ;  	   E  
   O     Z     w     �     �     �     �     �     �     �     �     �  
   �               #     3     C     _     m     �     �     �     �     �     �     �     �     �       "        0     8     D     T  	   `     j     s     z     �     �     �     �     �  &   �     �       #   (  C   L  K   �     �     �     �            5        K     S  -   q     �     �     �  	   �  	   �     �     �                      (   )     R     T     \  	   r  
   |     �     �     �     �     �     �     �     V   8       B   G          /   	   "   7   Q   T           d                 _   H                 
                          e       2   W   E   3   c          A       C   @          K   #       0      ]   O   4   b   &      -       Y   (   :   5       ^              =       +   I       a      Z              `   L             f      <      D   6       ?   F   U      *       N   )                  ;           >   '   J   [             1      !      P   S      .             %   \   R   X   M   9   ,      $       Add a card manually Add this card to the basket Add this card to your stock Add to the deposit: Add to… Alert of the Alertes Alerts All cards Aucun dépôt existant Authors Auto command Auto-command the cards Baskets Cancel Catalan Created the Deposit Deposits Dernier état de dépôt Diffuseur Distributor Distributors Dépôts existants Edit the card English Exemplaires restants Exemplaries in stock Français German History Inital quantity Inventories Key words Key-words Last deposit state Look for a Look for cards Minimum wanted quantity My stock New card New deposit New entry No card is this basket No results Nom du dépot Nouveau dépôt On the place Part of title, of author's name, etc. Places Presente in Public price Publishers Quantity Quantity left Search Search in the stock See on Sell Sell successfull. Selling cards Start an inventory The card "{}" was added successfully. The card doesn't exist. The card was created successfully. The deposit was successfully created. The deposit wasn't created. It must contain at least one valid card The search by ean isn't implemented yet. This card already exists. Title To command Type Type of card Use TAB to validate your choice, Enter to proceed. Validate Warning: we couldn't sell {}. Woops, we can not create this card. This is a bug ! about the card add to deposit cards conflict continue... deposit state distributor homepage in stock never new card of new entries, sells and movements or places produit indisponible publisher results source this card has no isbn titles selected. Total: valider voir en texte whose distributor is year of publication Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-30 10:11-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Añadir una ficha manualmente Añadir esta ficha en la cesta Añadir esta ficha en tu stock Añadir al depósito Añadir en… Alerta del Alertas Alertas todas las fichas No existe depósitos Autores Pedida auto Pedir automaticamente Cestas Cancelar Catalan Creada el Depósito Depósitos Último balance de depósito Difusora Difusora Difusora Depósitos existantes Editar la ficha Ingles Ejemplares sobrando Ejemplares en el stock Frances Alemano Histórico Quantidad inicial Inventarios Palabras claves Palabras claves Ultimo balance de depósito Buscar para:  Buscar para fichas Quantidad mínima Mi stock Nueva ficha Nuevo depósito Nueva ficha No hay ficha en esta cesta No resultados Nome Nuevo depósito En el lugar Parte del título, del autor, etc. Lugares Presente en Precio público Editoriales Quantidad Sobrando Buscar Buscar en el stock Ver en Vender La venta sucedió Vendendo Iniciar un inventario La ficha "{}" fue añadida con sucesso Esta ficha no existe La ficha fue creada con sucesso El depósito fue creado con sucesso El depósito no fue creado. La fichas eleccionadas no estan validas Disculpe, todavia no se puede buscar con el número isbn. Solo preguntar ;) Esta ficha ya existe. Título A pedir Tipo Tipo de ficha Usa TAB para seleccionar, Enter para validar la venta Validar Cuidada: no podemos vender {} Opa, no podemos crear esta ficha. Es un bug ! a proposito de la ficha añadir al depósito fichas conflicto seguir... balance de depósito difusora sitio en el stock nunca nueva ficha de nuevas entradas, ventas y movimientos o lugares producto indisponible editorial resultados fuente esta ficha no tiene isbn títulos seleccionados. Total: validar ver como texto cuya difusora es año de publicación 